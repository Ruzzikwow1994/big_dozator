/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FLASH_H
#define __FLASH_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

#define FLASH_PAGE_SIZE                 0x00000800U    /*!< FLASH Page Size, 2 KBytes */
#define FLASH_USER_START_ADDR   (FLASH_BASE + (14 * FLASH_PAGE_SIZE))

union Params Flash_Read (void);
void Flash_Write(union Params data);
#endif

