/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define HV_LAMP_Pin GPIO_PIN_15
#define HV_LAMP_GPIO_Port GPIOC
#define SENSIV_Pin GPIO_PIN_0
#define SENSIV_GPIO_Port GPIOA
#define AMOUNT_Pin GPIO_PIN_1
#define AMOUNT_GPIO_Port GPIOA
#define SERVICE_BUTTON_Pin GPIO_PIN_2
#define SERVICE_BUTTON_GPIO_Port GPIOA
#define SERVICE_BUTTON_EXTI_IRQn EXTI2_3_IRQn
#define MOTOR_HALT_Pin GPIO_PIN_4
#define MOTOR_HALT_GPIO_Port GPIOA
#define MOTOR_RUN_Pin GPIO_PIN_5
#define MOTOR_RUN_GPIO_Port GPIOA
#define LED_R_Pin GPIO_PIN_6
#define LED_R_GPIO_Port GPIOA
#define LED_B_Pin GPIO_PIN_7
#define LED_B_GPIO_Port GPIOA
#define LED_G_Pin GPIO_PIN_0
#define LED_G_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define BUTTON_DELAY 0.5 //������
#define MOTOR_ON HAL_GPIO_WritePin(MOTOR_RUN_GPIO_Port,MOTOR_RUN_Pin,GPIO_PIN_SET)
#define MOTOR_OFF HAL_GPIO_WritePin(MOTOR_RUN_GPIO_Port,MOTOR_RUN_Pin,GPIO_PIN_RESET)
#define BLUE_L  OFF_L; HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET) 
#define RED_L OFF_L; HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET)
#define GREEN_L  OFF_L; HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET)
#define OFF_L HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET); HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET); HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET)
#define UF_ON HAL_GPIO_WritePin(HV_LAMP_GPIO_Port, HV_LAMP_Pin, GPIO_PIN_SET)
#define UF_OFF HAL_GPIO_WritePin(HV_LAMP_GPIO_Port, HV_LAMP_Pin, GPIO_PIN_RESET)
#pragma anon_unions;
union states {
	uint16_t byte;	
	enum 
	{
		s0, //��������
		s1, //��������
		s2,	//������
		s3, //���������

	} states;
}; 

typedef enum 
{
	GREEN,
	RED
} led_color;

struct Flags{
	_Bool  opto_sensor;
	_Bool  button;
	_Bool	 service_mode;
	_Bool	 motor_timer;
	_Bool  sw_state;
	_Bool  def_reset;
	_Bool  need_save;
};
union Params {
	uint64_t byte;
struct {
	float  pump_delay;
	int	 opto_delay;
};
};

typedef enum  {
	PUMP,
	SWITCH_STATE,
	RESET_TIM,
	OPTO,
	OPTO_COUNTDOWN,
	HALT_COUNTDOWN,
	enum_size
} flag_name;
#define  transmitChar 0x55
#define sw_state_time 1
#define sw_state_time_long 2
extern struct Flags flags;
extern union Params Params;
void flag_delay (flag_name flg); 
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
