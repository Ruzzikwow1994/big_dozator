#include "fsm.h"

int true_delay;

union states FSM[8][4] = {
// s0		 s1    s2			s3			

	 {s0,	 s1,	 s0,		s3,},
	 {s3,	 s3,	 s3,		s0,},		 
	 {s0,	 s0,	 s0,		s3,},
	 {s3,	 s3,	 s3,		s0,},
	 {s1,	 s1,	 s2,		s3,},
	 {s3,	 s3,	 s3,		s0,},
	 {s1,	 s2,	 s2,		s3,},
	 {s3,	 s3,	 s3,		s0,},
	
};


//������� ����������
 union states current_state;
//������� ���������� ��� �������� �������
 union states check_state;


void uf(_Bool x)
{
	(x==1)? UF_ON : UF_OFF;
}


uint16_t Get_State_Cup(void)
{
	return current_state.byte;
}
static void state_s0 (void)
{
	//����� ������� 
	flag_delay(RESET_TIM);
	//�� ��������
	uf(0);
	//�����
	BLUE_L;
	
	Params.opto_delay=true_delay;
}

static void state_s1 (void)
{

	//�������� ����� ���������
	flag_delay(SWITCH_STATE);
	//�������� ��������
	flag_delay(PUMP);
	// ��������
	uf(1);
	//�����
	BLUE_L;
	
	Params.opto_delay=true_delay;
}

static void state_s2 (void)
{

	//����� ������
	flag_delay(RESET_TIM);
	//�� ��������
	uf(0);
	//�������
	RED_L;
	
	Params.opto_delay=1;
}

static void state_s3 (void)
{
	//����� ������
	flag_delay(RESET_TIM);
	//�� ��������
	uf(0);
	//�������
	GREEN_L;
	//���� ���������
	flags.service_mode=1;
	
	Params.opto_delay=true_delay;
}



static uint8_t getSignal(void)
{
	uint8_t num_state=0;
	//�������� � num_state ������� �� ������� � ������� ���������

	if(flags.opto_sensor) //������
	{
		
		num_state+=4; // 2^2
	}
	if(flags.sw_state) //������
	{
		num_state+=2; // 2^1
		flags.sw_state=0; //����� ����� ������� ����� �������������
	}

	if(flags.button) //������ ��������
	{
		num_state++; //2^0
		flags.button=0;  //����� �����
	}
	
	
	return num_state;
}

void doFSM_table(void)
{
	//��������� �������� ���������� �� �������
	  uint8_t current_signal = getSignal();
	//��������� ���������� ��������� ������ �� �������� ��������� � �������� ����������
		current_state = FSM[current_signal][current_state.states];
	//������ ���� ���������� �� ���������� ����� ����� ��� �� ��������� (����� ���������� ����� �� While(1))
	if(current_state.states!=check_state.states) 
		{
			if(check_state.states==s3)//���� ��������� ���������
			{
				flags.need_save=1;
			}
			check_state = current_state;
			switch(current_state.states)
			{
				case s0: 
					state_s0();
					break;
				case s1: 
					state_s1();
					break;
				case s2: 
					state_s2();
					break;
				case s3:
					state_s3();
					break;

			}
		}
}

void FSM_init(void)
{
	current_state.states = s0;
	check_state.states = 0xFF;
	true_delay = Params.opto_delay;
}


