/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "fsm.h"
#include "flash.h"
#include "switches.h"
#include "iwdg.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t recieveChar;
struct Flags flags={0};
union Params Params={0};
int motor_timer;
int motor_time = 1000;
int button_timer ;
uint32_t adc_result[32];
 int adcResP;
 int fadcResP;
 int adcResL;
 int fadcResL;
int cnt_ms;
int tikstart[enum_size];
uint8_t trCh = transmitChar;
extern int good_cnt;
extern int k;
extern _Bool colibr;
int colibr_cnt;
//������� ����������
extern union states current_state;
//������� ���������� ��� �������� �������
extern union states check_state;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void flag_delay (flag_name flg); 
void params_init(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_RTC_Init();
  MX_TIM3_Init();
	params_init();
  MX_USART1_UART_Init();
	//MX_IWDG_Init();
  /* USER CODE BEGIN 2 */
	
	FSM_init();
	HAL_TIM_Base_Start(&htim3);
	HAL_UART_Receive_IT(&huart1,&recieveChar,1);
	HAL_ADC_Start_DMA(&hadc1,(uint32_t *)&adc_result[0],32);
  /* USER CODE END 2 */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		//HAL_IWDG_Refresh(&hiwdg);
		if(flags.def_reset) //����� � ������ ����� 5 ��� ������� ������
		{
			flags.def_reset=0;
			Params.opto_delay = 20;
			Params.pump_delay = 0.4;
			flags.need_save=1;
			flags.service_mode=0;
			HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET); 
			HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
			HAL_Delay(500);
		}
		if(flags.need_save)
		{
			flags.need_save=0;
			HAL_Delay(100);
			Flash_Write(Params);
			HAL_Delay(500);
			HAL_NVIC_SystemReset();
		}

		flag_delay(enum_size); //��� ���������� �������� ���� ������
		
		if(colibr) doFSM_table();
		
		
		sw_Check(); //����� ����������
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_SYSCLK;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void HAL_SYSTICK_Callback(void)
{

	if(!colibr)cnt_ms++;
	
	HAL_UART_Transmit_IT (&huart1,&trCh,1); //11 ���� � ������� ������ �������� ��������

	if(flags.motor_timer)
	{
		MOTOR_ON;
	}
	else
	{
		MOTOR_OFF;
		flag_delay(HALT_COUNTDOWN);
	}
	
	if(cnt_ms>500)
	{
		cnt_ms=0;
		if(k<8)
		{
			k++;
			colibr_cnt=0;
			MX_USART1_UART_Init();
			HAL_UART_Receive_IT(&huart1,&recieveChar,1);
		}
		else
		{
			k=7;
			colibr=1;
			MX_USART1_UART_Init();
			HAL_UART_Receive_IT(&huart1,&recieveChar,1);
		}
	}
}






void flag_delay (flag_name flg) //�������� � While(1) � �����
{
	switch (flg) //� ����������� �� 
	{
		case PUMP:
			if(!tikstart[PUMP])	
			tikstart[PUMP] = HAL_GetTick();
			flags.motor_timer=1;
			break;
		
		case SWITCH_STATE:
			if(!tikstart[SWITCH_STATE])	
			tikstart[SWITCH_STATE] = HAL_GetTick();			
			break;
			

		case RESET_TIM:
			tikstart[PUMP] = 0;
			tikstart[SWITCH_STATE] = 0;
			break;

		case OPTO:
				
			tikstart[OPTO] = HAL_GetTick();
			break;
		
		case OPTO_COUNTDOWN:
			tikstart[OPTO_COUNTDOWN] = HAL_GetTick();
			break;
		
		case HALT_COUNTDOWN:
			tikstart[HALT_COUNTDOWN] = HAL_GetTick();
			HAL_GPIO_WritePin(MOTOR_HALT_GPIO_Port,MOTOR_HALT_Pin,GPIO_PIN_SET);
			break;
		
		case enum_size:			//� ���� ������ �������� � while(1) �������������� ����(���� ������� enum)
			for(int i=0; i<enum_size; i++)
			{
				if(tikstart[i]!=0)
				{
					switch(i)
					{
						case PUMP:
							if((HAL_GetTick() - tikstart[PUMP]) > Params.pump_delay*1000)  //��������  �����
							{
								flags.motor_timer = 0;	
								tikstart[PUMP] = 0;
							}
							break;
							
						case SWITCH_STATE:
							if((HAL_GetTick() - tikstart[SWITCH_STATE]) > sw_state_time*1000)  //�������� �������� � ���������
							{
								flags.sw_state=1;	
								tikstart[SWITCH_STATE] = 0;
							}
							break;	

							
						case OPTO:
							if((HAL_GetTick() - tikstart[OPTO]) > 2900)  //�������� ��������
							{
								flags.opto_sensor=0;	
								tikstart[OPTO] = 0;
							}
							break;

						case OPTO_COUNTDOWN:
							if((HAL_GetTick() - tikstart[OPTO_COUNTDOWN]) > 700*Params.opto_delay)  //�������� ��������
							{
								good_cnt=0;	
								tikstart[OPTO_COUNTDOWN] = 0;
							}
							break;	

						case HALT_COUNTDOWN:
							if((HAL_GetTick() - tikstart[HALT_COUNTDOWN]) > 1000)  //�������� ��������
							{
								HAL_GPIO_WritePin(MOTOR_HALT_GPIO_Port,MOTOR_HALT_Pin,GPIO_PIN_RESET);
								tikstart[HALT_COUNTDOWN] = 0;
							}
							break;
					}
				}
			}
			
			break;
	}
}

void params_init (void)
{
	union Params def;
	
	def.pump_delay = 0.4; //���
	def.opto_delay = 20;
	
	Params = Flash_Read();
	if(Params.byte==0xFFFFFFFFFFFFFFFF)
	{
		Params = def;
	}
}



/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
