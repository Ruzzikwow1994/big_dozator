#include "switches.h"


 struct {
	uint16_t pin;
	uint32_t tick;
} switches[SW_SIZE] = 
{
	{BUTTON,0},
	{BUTTON,0}

};
static void sw_AddCheckPUSH(uint16_t GPIO_Pin, uint32_t curr_tick)
{

			switches[0].tick = curr_tick;
		
}

static void sw_AddCheckPULL(uint16_t GPIO_Pin, uint32_t curr_tick)
{

			switches[1].tick = curr_tick;
		
}

void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{
	// �������� ��� ��������� �������� ���� ��������� ����� ����-���
	sw_AddCheckPUSH(GPIO_Pin, HAL_GetTick());
	
}

void HAL_GPIO_EXTI_Rising_Callback(uint16_t GPIO_Pin)
{
	sw_AddCheckPULL(GPIO_Pin, HAL_GetTick());
}

static void sw_Action(uint16_t GPIO_Pin)
{
	if(HAL_GPIO_ReadPin(SERVICE_BUTTON_GPIO_Port, SERVICE_BUTTON_Pin) == GPIO_PIN_SET)  //������ ������
		{
			flags.button = 1;
		}
		else
		{
			flags.button = 0;
		}
		
}
#define LONG_TIMEOUT 5 //������
#define TIMEOUT 1 //������
void sw_Check()
{
		if(switches[1].tick != 0)
		{
			if(switches[1].tick-switches[0].tick  > LONG_TIMEOUT*1000)
			{
				switches[0].tick = 0;
				switches[1].tick = 0;
				flags.def_reset=1;	
			}
			else if(switches[1].tick-switches[0].tick  > TIMEOUT*1000)
			{
				switches[0].tick = 0;
				switches[1].tick = 0;
				sw_Action(switches[0].pin);
			}
			else
			{
				switches[0].tick = 0;
				switches[1].tick = 0;
			}
			
		}
	
}
