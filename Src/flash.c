/**
  ******************************************************************************
  * @file    storage.c
  * @author  sdaware@mac.com
  * @version V1.0.0
  * @date    14.09.2020
  * @brief   ������ � ������� �� EEPROM.������ �������� 64 �����.
  ******************************************************************************
  */

#include "main.h"
#include "flash.h"

uint32_t FirstPage = 0, NbOfPages = 0;
uint32_t Address = 0, PageError = 0;
__IO uint32_t data32 = 0 , MemoryProgramStatus = 0;

/*Variable used for Erase procedure*/
static FLASH_EraseInitTypeDef EraseInitStruct = {0};

static uint32_t GetPage(uint32_t Address);

union Params Flash_Read (void)
{
	union Params data;
	data.byte =( *(__IO uint64_t *)FLASH_USER_START_ADDR);
	return data;
}

// ���������� ���������� � ���������
void Flash_Write(union Params data)
{

	HAL_FLASH_Unlock();
  

  /* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/


  /* Get the 1st page to erase */
  FirstPage = GetPage(FLASH_USER_START_ADDR);

  /* Get the number of pages to erase from 1st page */
  NbOfPages = 1;

  /* Fill EraseInit structure*/
  EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
  EraseInitStruct.Page        = FirstPage;
  EraseInitStruct.NbPages     = NbOfPages;

  /* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
     you have to make sure that these data are rewritten before they are accessed during code
     execution. If this cannot be done safely, it is recommended to flush the caches by setting the
     DCRST and ICRST bits in the FLASH_CR register. */
  if (HAL_FLASHEx_Erase(&EraseInitStruct, &PageError) != HAL_OK)
  {
		if (HAL_FLASHEx_Erase(&EraseInitStruct, &PageError) != HAL_OK)
		{
				while(1){}
		}
		
	}
						
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FLASH_USER_START_ADDR, data.byte );

	HAL_FLASH_Lock();
	
}


static uint32_t GetPage(uint32_t Addr)
{
  return (Addr - FLASH_BASE) / FLASH_PAGE_SIZE;;
}
